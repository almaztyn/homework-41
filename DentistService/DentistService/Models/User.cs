using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DentistService.Interfaces;

namespace DentistService.Models
{
    public class User : IEntity<int>
    {
       public int Id { get; set; }
       
       [ForeignKey("Organization")]
       public int OrganizationId { get; set; }
       [Required]
       public string Login { get; set; }
       
       [Required]
       public string FullName { get; set; }
       
       [Required]
       public string Password { get; set; }
       public bool IsBanned { get; set; }
       
       [DataType(DataType.Date)]
       public DateTime LoginCreationDate { get; set; }
       
       public virtual ICollection<Visit> Visits { get; set; } = new List<Visit>();
       public virtual Organization Organization { get; set; }
    }
}