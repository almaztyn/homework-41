﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DentistService.Enums;
using DentistService.Interfaces;

namespace DentistService.Models
{
    public class Visit : IEntity<int>
    {
        public int Id { get; set; }
        
        [ForeignKey("Patient")]
        public int PatientId { get; set; }
        
        public int UserId { get; set; }
        public int DentalCardId { get; set; }
        [Required]
        public VisitType VisitType { get; set; }
        public DateTime VisitDate { get; set; }
        
        [Required]
        public VisitStatus Status { get; set; }
        public decimal TotalPrice { get; set; }
        public virtual Patient Patient { get; set; }
        public virtual DentalCard DentalCard { get; set; }
        public virtual User User { get; set; }
    }
}