using System.Collections;
using System.Collections.Generic;
using DentistService.Interfaces;

namespace DentistService.Models
{
    public class DentalCard : IEntity<int>
    {
       public int Id { get; set; } 
       public int ToothId { get; set; }
       public int VisitId { get; set; }
       public string Information { get; set; }
       public string Comment { get; set; }

       public virtual Tooth Tooth { get; set; } 
       public virtual Visit Visit { get; set; } 
    }
}