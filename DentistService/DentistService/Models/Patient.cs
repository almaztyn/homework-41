﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DentistService.Enums;
using DentistService.Interfaces;

namespace DentistService.Models
{
    public class Patient : IEntity<int>
    {
        public int Id { get; set; }

        [ForeignKey("Organization")] 
        public int OrganizationId { get; set; }
        
        [Required] 
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }
        public string? MiddleName { get; set; }
        
        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }
        
        [Required] 
        public Sex Sex { get; set; }

        public DateTime LoginCreationDate { get; set; }
        public virtual Organization Organization { get; set; }
        public virtual ICollection<Visit> Visits { get; set; } = new List<Visit>();
    }
}