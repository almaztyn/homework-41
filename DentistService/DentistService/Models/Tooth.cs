using System.ComponentModel.DataAnnotations;
using DentistService.Enums;
using DentistService.Interfaces;

namespace DentistService.Models
{
    public class Tooth : IEntity<int>
    {
        public int Id { get; set; }
        public int DentalCardId { get; set; } 
        [Required]
        public Jaw Jaw { get; set; }
        
        [Required]
        public JawSide JawSide { get; set; }
        
        [Required]
        public int ToothNumber { get; set; }
        public virtual DentalCard DentalCard { get; set; }
    }
}