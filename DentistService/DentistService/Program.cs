﻿using System;
using System.Linq;
using DentistService.Enums;
using DentistService.Models;
using Microsoft.EntityFrameworkCore;

namespace DentistService
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new ApplicationDbContext())
            {
                Seed(db);
                var orgs = db.Organizations
                    .Include(p => p.Patients)
                    .ThenInclude(v => v.Visits);

                foreach (var org in orgs)
                {
                    Console.WriteLine($"Organization Name: {org.Name} / Director: {org.Director}");
                    Console.WriteLine($"Patients List: ");
                    foreach (var p in org.Patients)
                    {
                        Console.WriteLine("------------------------------------");
                        Console.WriteLine($"Fullname: {p.FirstName} {p.LastName}");
                        Console.WriteLine($"Date of Birth: {p.DateOfBirth}");
                        Console.WriteLine($"Login Created: {p.LoginCreationDate}");
                        Console.WriteLine($"Patient's Visit List: ");
                        foreach (var v in p.Visits)
                        {
                            Console.WriteLine("***********************************");
                            Console.WriteLine($"Visit Type: {v.VisitType}");
                            Console.WriteLine($"Visit Date: {v.VisitDate}");
                            Console.WriteLine($"Visit Status: {v.Status}");
                            Console.WriteLine($"Total Price: {v.TotalPrice}");
                        }
                        Console.WriteLine("------------------------------------");
                    }
                }
            }
        }

        public static void Seed(ApplicationDbContext db)
        {
            if (!db.Organizations.Any())
            {
                var org1 = new Organization
                {
                    Name = "NovaDent",
                    Address = "Bishkek, Mir str.",
                    Director = "John Skada"
                };

                var patient1 = new Patient
                {
                    FirstName = "Alladin",
                    LastName = "Musk",
                    MiddleName = "Junior",
                    DateOfBirth = new DateTime(1985, 02, 24),
                    LoginCreationDate = DateTime.Now.Date,
                    Sex = Sex.Male,
                    Organization = org1,
                };
                var patient2 = new Patient
                {
                    FirstName = "Jibek",
                    LastName = "Jolie",
                    MiddleName = "Elizabeth",
                    DateOfBirth = new DateTime(1998, 01, 05),
                    LoginCreationDate = DateTime.Now.Date,
                    Sex = Sex.Female,
                    Organization = org1
                };

                var visit1 = new Visit
                {
                    Patient = patient1,
                    VisitType = VisitType.Consultation,
                    VisitDate = new DateTime(2022, 03, 15),
                    Status = VisitStatus.Scheduled,
                    TotalPrice = 1000
                };
                
                var visit2 = new Visit
                {
                    Patient = patient2,
                    VisitType = VisitType.Treatment,
                    VisitDate = new DateTime(2022, 03, 12),
                    Status = VisitStatus.Visited,
                    TotalPrice = 5800
                };
                
                var visit3 = new Visit
                {
                    Patient = patient2,
                    VisitType = VisitType.Surgery,
                    VisitDate = new DateTime(2022, 03, 16),
                    Status = VisitStatus.Scheduled,
                    TotalPrice = 8400
                };

                db.Visits.Add(visit1);
                db.Visits.Add(visit2);
                db.Visits.Add(visit3);
                db.SaveChanges();
            }
        }
    }
}