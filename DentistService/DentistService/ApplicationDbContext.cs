﻿using DentistService.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DentistService
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Visit> Visits { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(@"D:\C - SHARP\GitRepositories\homework-41\DentistService\DentistService");
            builder.AddJsonFile("appsettings.json");
            var config = builder.Build();
            var connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Organization>().HasIndex(x => x.Name).IsUnique();
            
            modelBuilder.Entity<User>().HasIndex(x => x.Login).IsUnique();
            
            modelBuilder.Entity<DentalCard>()
                .HasOne(x => x.Tooth)
                .WithOne(x => x.DentalCard)
                .HasForeignKey<Tooth>(x => x.DentalCardId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<DentalCard>()
                .HasOne(x => x.Visit)
                .WithOne(x => x.DentalCard)
                .HasForeignKey<Visit>(x => x.DentalCardId)
                .OnDelete(DeleteBehavior.NoAction);
            
            modelBuilder.Entity<Visit>()
                .HasOne(x => x.User)
                .WithMany(x => x.Visits)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Organization>()
                .HasMany(x => x.Users)
                .WithOne(x => x.Organization)
                .HasForeignKey(x => x.OrganizationId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}