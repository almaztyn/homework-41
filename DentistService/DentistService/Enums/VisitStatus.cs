﻿namespace DentistService.Enums
{
    public enum VisitStatus
    {
        Scheduled = 1,
        Visited = 2,
        Missed = 3
    }
}