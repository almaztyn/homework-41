namespace DentistService.Enums
{
    public enum JawSide
    {
        Right = 1,
        Left = 2
    }
}