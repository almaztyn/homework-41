namespace DentistService.Enums
{
    public enum Jaw
    {
        Maxilla = 1,
        Mandible = 2
    }
}