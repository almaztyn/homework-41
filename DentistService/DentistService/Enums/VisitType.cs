﻿namespace DentistService.Enums
{
    public enum VisitType
    {
        Consultation = 1,
        Treatment = 2,
        Surgery = 3,
    }
}