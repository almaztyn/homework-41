﻿namespace DentistService.Enums
{
    public enum Sex
    {
        Male = 1,
        Female = 2,
    }
}